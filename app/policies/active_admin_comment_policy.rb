class ActiveAdminCommentPolicy  < AdminPolicy

  attr_reader :user, :model

  def initialize(user, model)
    @user = user
    @model = model
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      if @user and @user.role? :admin
        scope.all
      else
        scope.approved
      end
    end
  end


  def create?
    true
  end

  def index?
    true
  end

  def show?
    true
  end
end
