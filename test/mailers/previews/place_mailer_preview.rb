# Preview all emails at http://localhost:3000/rails/mailers/place_mailer
class PlaceMailerPreview < ActionMailer::Preview
  def pending_notification
    PlaceMailer.pending_notification(Place.first)
  end
end
