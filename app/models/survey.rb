class Survey < ActiveRecord::Base
  scope :ongoing, ->{ where("start_at <= ? AND ? < end_at", Time.now, Time.now) }

  def ongoing?
    start_at <= Time.now and Time.now < end_at
  end

  def excerpt
    ActionView::Base.full_sanitizer.sanitize(body).truncate(140)
  end
end
