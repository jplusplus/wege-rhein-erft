class AddStatusToActiveAdminComment < ActiveRecord::Migration
  def up
    add_column :active_admin_comments, :status, :string, default: "pending", null: false
  end

  def down
    remove_column :active_admin_comments, :status
  end
end
