ActiveAdmin.register Place do
  permit_params do
    VariableController.names.map(&:to_sym) + [:accessibility, :status, :picture, :kategorie]
  end

  menu priority: 1
  filter :name
  filter :status, :as => :select, :collection => Place.status.options

  scope :all
  scope :pending, ->(scope){ scope.pending }

  active_admin_import(
    validate: false,
    csv_options: {
      skip_blanks: true
    },
    before_batch_import: ->(importer) {
      # Importing places is a destructing action
      Place.delete_all
    }
  )

  batch_action :approve do |ids, inputs|
    Place.find(ids).each do |place|
      place.status = :approve
      place.save
    end
    redirect_to collection_path, :flash => { :success =>  "#{ids.length} place(s) approved!" }
  end

  batch_action :reject do |ids, inputs|
    Place.find(ids).each do |place|
      place.status = :reject
      place.save
    end
    redirect_to collection_path, :flash => { :success =>  "#{ids.length} place(s) rejected!" }
  end


  index do
    selectable_column
    id_column
    column :name
    tag_column :status
    column :emails
    column :address
    column :latitude
    column :longitude

    actions
  end

  form do |f|
    f.inputs "Place Details" do
      # Collect field from Place columns
      columns = Place.columns.map(&:name).map(&:to_sym)
      # Remove a few field
      columns = columns.reject { |key| [:id, :kategorie, :accessibility, :created_at, :updated_at].include? key }
      # Create an input for each remaining column
      columns.each do |name|
        f.input name
      end
      # Add the special input
      f.input :kategorie, as: :tags, collection: PlaceCategoryController.names
      f.input :accessibility, as: :tags, collection: PlaceAccessibilityController.names
    end
    actions
  end


  csv force_quotes: true do
    Place.columns.map(&:name).each do |name|
      column name.to_sym, humanize_name: false
    end
  end
end
