module API
  class PlaceAccessibilities < Grape::API
    resource :place_accessibilities do

      desc "Return a list of accessibilities"
      get do
        PlaceAccessibilityController::uniq.map do |pc|
          { name: pc.name, picture: pc.picture }
        end
      end

    end
  end
end
