ActiveAdmin.register Input do
  permit_params do
    policy(Input).permitted_attributes
  end

  active_admin_import(
    validate: false,
    csv_options: {
      skip_blanks: false
    },
    before_batch_import: ->(importer) {
      # Importing places is a destructing action
      Input.delete_all
    }
  )

  config.filters = false
  config.sort_order = 'rank_asc'
  sortable tree: false, sorting_attribute: :rank

  index do
    selectable_column
    id_column
    column :label
    column :help_text
    column :group
    column :rank
    actions
  end

  index as: :sortable do
    label :label
    actions
  end

  form do |f|
    f.inputs "Admin Details" do
      policy(Input).permitted_attributes.each do |attribute|
        f.input attribute
      end
    end
    f.actions
  end
end
