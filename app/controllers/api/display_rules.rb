module API
  class DisplayRules < Grape::API
    resource :display_rules do

      desc "Return a list of display rules"
      get do
        DisplayRuleController::all.map do |rule|
          {
            label: rule.label,
            type:  rule.type,
            rank: rule.rank,
            group: rule.group,
            variable: rule.variable
          }
        end
      end

    end
  end
end
