class AddStatusToPlace < ActiveRecord::Migration
  def up
    add_column :places, :status, :string, default: "approve", null: false
  end

  def down
    remove_column :places, :status
  end
end
