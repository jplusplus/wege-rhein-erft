angular.module 'wre'
  .config ($stateProvider) ->
    $stateProvider
      .state 'main.place',
        templateUrl: 'main/place/place.html'
        controller: 'MainPlaceCtrl'
        controllerAs: 'place'
        url: 'places/:id'
        resolve:
          place: ($stateParams, Restangular, filters)->
            'ngInject'
            Restangular.one('places', $stateParams.id).get().then (p)->
              # Look for the place accessibilities
              p.accessibility_list = _.map p.accessibility_list, (accessibility)->
                _.find filters.accessibilities, name: accessibility
              p
