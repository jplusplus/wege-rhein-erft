angular.module 'wre'
  .config ($translateProvider)->
    'ngInject'
    $translateProvider
      .useStaticFilesLoader
        prefix: '/locales/',
        suffix: '.json'
      .registerAvailableLanguageKeys ['de'],
        'de_DE': 'de'
      .determinePreferredLanguage ->
        lang = navigator.language or navigator.userLanguage
        avalaibleKeys = [ 'de_DE', 'de' ]
        if avalaibleKeys.indexOf(lang) is -1 then 'de' else lang
      .fallbackLanguage ['de']
      .useLocalStorage()
      .useSanitizeValueStrategy null
