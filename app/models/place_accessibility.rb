class PlaceAccessibility
  include ActiveModel::Model

  attr_accessor :name, :group, :variables, :picture

  def variables
    @variables.split(',')
  end

  def groups
    groups = PlaceAccessibilityController::all.select { |s| s.name == @name }
    groups.map{ |c| c.group }.uniq
  end
end
