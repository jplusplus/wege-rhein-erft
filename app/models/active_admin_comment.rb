class ActiveAdminComment < ActiveRecord::Base
  extend Enumerize
  belongs_to :resource, polymorphic: true
  belongs_to :author,   polymorphic: true
  after_create :send_pending_notification

  enumerize :status, :in => [:pending, :approve, :reject], :default => :pending, :predicates => true

  scope :approved, ->{ where(:status => :approve) }
  scope :pending, ->{ where(:status => :pending) }

  def author_ip_hash=(ip)
    write_attribute :author_ip_hash, Digest::MD5.hexdigest(ip)
  end

  def author_name
    if is_anonymous
      read_attribute(:author_name)
    else
      author.name
    end
  end

  def author_avatar
    if is_anonymous or not author.email
      hash = Digest::MD5.hexdigest author_ip_hash + author_name
    else
      hash = Digest::MD5.hexdigest author.email
    end
    '//github.com/identicons/' + hash + '.png'
  end

  def is_anonymous
    not author
  end

  def author_or_name
    if is_anonymous
      read_attribute(:author_name)
    else
      author
    end
  end

  protected
    def send_pending_notification
      if status == :pending
        ActiveAdminCommentMailer.pending_notification(self).deliver_now
      end
    end
end
