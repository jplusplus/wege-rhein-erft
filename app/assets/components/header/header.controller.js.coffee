angular.module 'wre'
  .controller 'HeaderCtrl', (menu, filters, $scope, $translate)->
    new class HeaderCtrl
      constructor: ->
        # Configurable menu instance
        @menu = menu
        @filters = filters
        # Build the menu with filters
        for accessibility in filters.accessibilities
          @menu.addItem
            # A menu item HAS a section
            name: accessibility.name
            section: $translate.instant('header.filter.accessibility')
            order: 0
            icon: accessibility.picture
            onClick: -> filters.toggleAccessibility @name
            isActive: -> filters.isActiveAccessibility @name
        # Build the menu with filters
        for category in filters.categories
          @menu.addItem
            # A menu item HAS a category
            name: category.name
            section: $translate.instant('header.filter.category')
            order: 10
            onClick: -> filters.toggleCategory @name
            isActive: -> filters.isActiveCategory @name
        # Watch for state changes
        $scope.$on '$stateChangeStart', menu.hide
