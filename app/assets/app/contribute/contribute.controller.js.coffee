angular.module 'wre'
  .controller 'ContributeCtrl', (inputs, growl, $translate, Restangular)->
    new class ContributeCtrl
      constructor: ->
        # Place to send
        @place = {}
        # True if the contribute form is frozen
        @frozen = no
        # Split values list
        inputs = _.map inputs, (input)->
          input.values = input.values.split(',') if input.values?
          input
        @groups = _.groupBy _.sortBy(inputs, 'rank'), 'group'
      send: ->
        # Freeze the form
        @frozen = yes
        # Send the data
        Restangular.all('places').post(@place).then =>
          # Unfreeze the form
          @frozen = no
          # Reset the form
          @place = {}
          # Thanks the user
          growl.success $translate.instant('contribute.thanks')
        , (res)=>
          # Unfreeze the form
          @frozen = no
          # Notificate the user
          growl.error res.data.error or $translate.instant('contribute.error')
