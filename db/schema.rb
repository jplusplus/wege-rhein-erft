# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161215145454) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",                        null: false
    t.string   "resource_type",                      null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status",         default: "pending", null: false
    t.string   "author_name",    default: ""
    t.string   "author_ip_hash", default: "",        null: false
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"

  create_table "inputs", force: :cascade do |t|
    t.string  "label"
    t.string  "help_text"
    t.string  "variable"
    t.string  "variable_type"
    t.string  "group"
    t.string  "values"
    t.string  "parent"
    t.integer "rank"
    t.boolean "required",      default: false
  end

  create_table "pages", force: :cascade do |t|
    t.string   "title",      default: "", null: false
    t.string   "slug",       default: "", null: false
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "places", force: :cascade do |t|
    t.string   "name"
    t.string   "telefon"
    t.string   "fax"
    t.string   "webseite"
    t.string   "email"
    t.string   "branche"
    t.string   "etage"
    t.string   "offnungszeiten"
    t.string   "adresse_or_plz"
    t.string   "adresse_or_strasse_und_hausnummer"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "ansprechspartner"
    t.boolean  "arzt_or_arztzimmer_behandlungsraume_grossraumig"
    t.boolean  "arzt_or_arztzimmer_behandlungsraume_stufenlos_erreichbar"
    t.boolean  "arzt_or_terminvereinbarung_fax_sms_email_oder_online_moglich"
    t.boolean  "arzt_or_untersuchungsmobel_armlehnen_wegklappbar"
    t.boolean  "arzt_or_untersuchungsmobel_hohenverstellbar_flexibel"
    t.boolean  "aufzug_or_akustische_sprachansage"
    t.boolean  "aufzug_or_aufzug"
    t.boolean  "aufzug_or_bedienknopfe_fur_rollstuhlfahrer_erreichbar"
    t.boolean  "aufzug_or_bedienknopfe_taktil_ertastbar"
    t.boolean  "aufzug_or_fahrkabinengrosse_mind_110x140_cm"
    t.boolean  "aufzug_or_rollstuhlgerechte_turbreite_mind_80cm"
    t.boolean  "aufzug_or_visuelle_anzeige"
    t.boolean  "bsm_or_hilfen_fur_menschen_mit_horbehinderungen"
    t.string   "bsm_or_hilfen_fur_menschen_mit_horbehinderungen_welche"
    t.boolean  "bsm_or_hilfen_fur_menschen_mit_sehbehinderungen"
    t.string   "bsm_or_hilfen_fur_menschen_mit_sehbehinderungen_welche"
    t.boolean  "bsm_or_hilfen_fur_menschen_mit_verstandnisschw"
    t.string   "bsm_or_hilfen_fur_menschen_mit_verstandnisschw_welche"
    t.string   "schwimmbad_or_anzahl_nassrollstuhle"
    t.boolean  "schwimmbad_or_behindertengerechte_toilette"
    t.boolean  "schwimmbad_or_bewegungsflache_mind_15_x_15m"
    t.boolean  "schwimmbad_or_bewegungsflache_neben_dem_klappsitz_mind95cm"
    t.boolean  "schwimmbad_or_dusch_klappsitz_m_ruckenlehne"
    t.boolean  "schwimmbad_or_einhebel_duscharmatur_mit_handbrause"
    t.boolean  "schwimmbad_or_einzelumkleidekabine_fur_behinderte_menschen"
    t.boolean  "schwimmbad_or_fest_montierter_lifter"
    t.boolean  "schwimmbad_or_flexibler_lifter"
    t.boolean  "schwimmbad_or_handgriffe"
    t.boolean  "schwimmbad_or_hygieneeimer_mit_deckel"
    t.boolean  "schwimmbad_or_kabine_mit_abschliessbaren_fachern"
    t.boolean  "schwimmbad_or_kabine_mit_euroschlussel_abschliessbar"
    t.boolean  "schwimmbad_or_kiosk_cafeteria_stufenlos_erreichbar"
    t.boolean  "schwimmbad_or_lifter_am_schwimmbecken"
    t.boolean  "schwimmbad_or_markierung_der_obersten_stufe"
    t.boolean  "schwimmbad_or_nassrollstuhle_ausleihbar"
    t.string   "schwimmbad_or_raum_zum_abschrecken_turbreite_mind_80_cm"
    t.boolean  "schwimmbad_or_raum_zum_abschrecken_tur"
    t.boolean  "schwimmbad_or_rollstuhlabstellplatz_mind_15_x_15m"
    t.string   "schwimmbad_or_ruheraum_turbreite_mind_80cm"
    t.boolean  "schwimmbad_or_ruheraum_stufenlos_erreichbar"
    t.boolean  "schwimmbad_or_sauna_vorhanden"
    t.boolean  "schwimmbad_or_saunakabine_platz_fur_einen_rollstuhl_95cm"
    t.string   "schwimmbad_or_saunakabinen_turbreite"
    t.boolean  "schwimmbad_or_see_mit_sandstrand_weg_zum_wasser_befestigt"
    t.boolean  "schwimmbad_or_separate_dusche"
    t.boolean  "schwimmbad_or_separate_sitzgelegenheit"
    t.boolean  "schwimmbad_or_treppen_eingangsbereich"
    t.string   "schwimmbad_or_treppen_ins_becken_anzahl_der_stufen"
    t.boolean  "schwimmbad_or_treppengelander_auf_beiden_seiten"
    t.boolean  "schwimmbad_or_wib_kontrastreiche_markierung"
    t.boolean  "schwimmbad_or_wib_sirene_und_oder_blinklicht_bei_gefahr"
    t.boolean  "schwimmbad_or_wib_taktile_elemente_zur_orientierung"
    t.boolean  "sportsstatte_or_ausstellungsraume_sind_stufenlos_erreichbar"
    t.boolean  "sportsstatte_or_barrierefreier_zugang_zur_sporthalle_statte"
    t.boolean  "sportsstatte_or_bewegungsflache_neben_dem_klappsitz_grosse"
    t.string   "sportsstatte_or_bewegungsflache_neben_dem_klappsitz_mind95cm"
    t.boolean  "sportsstatte_or_dusch_klappsitz_m_ruckenlehne"
    t.boolean  "sportsstatte_or_einhebel_duscharmatur_mit_handbrause"
    t.boolean  "sportsstatte_or_einzelumkleidekabine_fur_behinderte_menschen"
    t.boolean  "sportsstatte_or_handgriffe"
    t.boolean  "sportsstatte_or_hygieneeimer_mit_deckel"
    t.string   "sportsstatte_or_kabine_bewegungsflache_mind_15_x_15m"
    t.boolean  "sportsstatte_or_kabine_mit_abschliessbaren_fachern"
    t.boolean  "sportsstatte_or_kabine_mit_euroschlussel_abschliessbar"
    t.boolean  "sportsstatte_or_markierungen_auf_der_obersten_untersten_stufe"
    t.boolean  "sportsstatte_or_markierungen_der_obersten_und_untersten_stufe"
    t.boolean  "sportsstatte_or_rollstuhlabstellplatz_mind_15_x_15m"
    t.boolean  "sportsstatte_or_rollstuhlplatze_im_zuschauerraum_tribune"
    t.boolean  "sportsstatte_or_separate_dusche"
    t.boolean  "sportsstatte_or_separate_sitzgelegenheit"
    t.boolean  "sportsstatte_or_treppe"
    t.string   "sportsstatte_or_treppe_anzahl_der_stufen"
    t.boolean  "sportsstatte_or_treppen_zur_halle"
    t.string   "sportsstatte_or_treppen_zur_halle_anzahl_der_stufen_bis"
    t.boolean  "sportsstatte_or_treppengelander_auf_beiden_seiten"
    t.boolean  "sportsstatte_or_treppenlift_mit_plattform_belastbar_bis_300_kg"
    t.boolean  "sportsstatte_or_treppenlift_mit_stuhl_belastbar_bis_140_kg"
    t.boolean  "sportsstatte_or_warnmoglichkeiten_kontrastreiche_markierung"
    t.boolean  "sportsstatte_or_warnmoglichkeiten_sirene_blinklicht_bei_gefahr"
    t.boolean  "sportsstatte_or_warnmoglichkeiten_taktile_elemente_orientierung"
    t.string   "sportsstatte_or_wenn_nein_sind_stufenlos_erreichbar"
    t.boolean  "toiletten_or_bewegungsflache_mind_150_cm_tief"
    t.boolean  "toiletten_or_euroschlussel_abschliessbar"
    t.string   "toiletten_or_grosse"
    t.boolean  "toiletten_or_grossraumige_behindertengerechte_toilette"
    t.boolean  "toiletten_or_haltegriffe"
    t.string   "toiletten_or_turbreite"
    t.boolean  "toiletten_or_turbreite_mind_80_cm"
    t.string   "zugang_or_anzahl_der_stufen_bis_3_cm_zahlt_nicht_als_stufe"
    t.boolean  "zugang_or_automatische_tur"
    t.boolean  "zugang_or_behindertenparkplatz"
    t.boolean  "zugang_or_innenliegende_turen"
    t.boolean  "zugang_or_klingel_fur_rollstuhlfahrer_erreichbar"
    t.boolean  "zugang_or_manuelle_tur"
    t.boolean  "zugang_or_mobile_rampe_verfugbar"
    t.boolean  "zugang_or_niveaugleicher_zugang"
    t.boolean  "zugang_or_rampe_vorhanden"
    t.boolean  "zugang_or_stufen"
    t.boolean  "zugang_or_treppenlift_mit_plattform_belastbar_bis_300_kg"
    t.boolean  "zugang_or_treppenlift_mit_stuhl_belastbar_bis_140_kg"
    t.string   "zugang_or_innenliegende_turen_turbreite"
    t.string   "zugang_or_turbreite"
    t.boolean  "zugang_or_turbreite_mind_80cm"
    t.string   "toiletten_or_weitere_angabe"
    t.string   "sportsstatte_or_weitere_angabe"
    t.string   "schwimmbad_or_weitere_angabe"
    t.string   "zugang_or_weitere_angabe"
    t.string   "aufzug_or_weitere_angabe"
    t.string   "kategorie"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status",                                                          default: "approve", null: false
    t.string   "accessibility"
    t.string   "picture",                                                         default: ""
    t.string   "kommentar",                                                       default: ""
  end

  create_table "rich_rich_files", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "rich_file_file_name"
    t.string   "rich_file_content_type"
    t.integer  "rich_file_file_size"
    t.datetime "rich_file_updated_at"
    t.string   "owner_type"
    t.integer  "owner_id"
    t.text     "uri_cache"
    t.string   "simplified_type",        default: "file"
  end

  create_table "surveys", force: :cascade do |t|
    t.string   "title"
    t.text     "body"
    t.string   "redirect_url"
    t.date     "start_at"
    t.date     "end_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.string   "encrypted_password",     default: "",          null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,           null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,           null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "provider"
    t.string   "uid"
    t.string   "role",                   default: "anonymous", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",                   default: "",          null: false
  end

  add_index "users", ["provider"], name: "index_users_on_provider"

end
