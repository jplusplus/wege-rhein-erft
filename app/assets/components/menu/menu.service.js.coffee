angular.module 'wre'
  .service 'menu', ($state)->
    class MenuItem
      constructor: (attributes={})->
        @name       = attributes.name
        @order      = attributes.order or 0
        @icon       = attributes.icon
        @state      = attributes.state or null
        @href       = attributes.href
        @section    = attributes.section or null
        @priority   = attributes.priority or attributes.rank or 0
        @clickFn    = attributes.onClick
        @isActiveFn = attributes.isActive
      onClick: =>
        do @clickFn if @clickFn?
      isActive: =>
        if @isActiveFn?
          do @isActiveFn
        else if @state?
          $state.current.name is @state
      getHref: (params={})=>
        href  = @href or ''
        href += $state.href(@state, params) or ''
    class Menu
      constructor: ->
        @_items = []
        @_visible = no
      isVisible: =>
        @_visible
      toggle: =>
        @_visible = not @_visible
      show: =>
        @_visible = yes
      hide: =>
        @_visible = no
      hasTitle: =>
        @_title?
      getTitle: =>
        @_title
      setTitle: (title)=>
        @_title = title
      hasPrimaryColor: =>
        @_primaryColor?
      getPrimaryColor: =>
        @_primaryColor
      setPrimaryColor: (primaryColor)=>
        @_primaryColor = primaryColor
      getSections: (section)=>
        # Inspect items to findd categories
        _.chain(@_items).map('section').uniq().orderBy( (section)=>
          _.chain(@_items).filter(section: section).map('order').min().value()
        ).value()
      getItems: (section=null)=>
        _.chain(@_items).filter(section: section).sortBy('priority').reverse().value()
      addItem: (item)=>
        @_items.push new MenuItem(item) unless _.find @_items, name: item.name
    # Return a single instance of the menu
    new Menu
