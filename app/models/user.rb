require 'date'

class User < ActiveRecord::Base
  extend Enumerize
  # Include default devise modules. Others available are:
  devise :lockable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable,
         :database_authenticatable,
         :omniauthable, :omniauth_providers => [:google_oauth2, :facebook]

  has_many :active_admin_comments, as: :author
  enumerize :role, :in => [:admin, :editor, :contributor], :default => :contributor

  def to_s
    email
  end

  def as_json(arg)
    # Keep and dirty way to add email_md5  to user serialization
    default = { :methods => [:email_md5] }
    super default.deep_merge arg
  end

  def name
    if not n = read_attribute(:name) or n == ''
      anonymised_email
    else
      n
    end
  end

  def anonymised_email
    (email || '').gsub /[^@.]/, '*'
  end

  def email_md5
    Digest::MD5.hexdigest email
  end

  def email_required?
    false
  end

  def password_required?
    false
  end

  def password=(p)
    if p
      super
    end
  end

  def role?(*roles)
    roles.map(&:to_sym).include? role.to_sym
  end

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.name = auth.info.name
      user.confirmed_at = DateTime.new
    end
  end
end
