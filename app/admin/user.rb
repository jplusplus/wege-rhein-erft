require 'date'

ActiveAdmin.register User do
  permit_params :email, :password, :password_confirmation,
                :name, :role, :confirmed_at
  menu priority: 3


  batch_action :confirm_now do |ids, inputs|
    User.find(ids).each do |user|
      user.confirmed_at = DateTime.new unless user.confirmed_at
      user.save
    end
    redirect_to collection_path, :flash => { :success =>  "#{ids.length} user(s) confirmed!" }
  end

  batch_action :unconfirm do |ids, inputs|
    User.find(ids).each do |user|
      user.confirmed_at = nil
      user.save
    end
    redirect_to collection_path, :flash => { :success =>  "#{ids.length} users(s) unconfirmed!" }
  end


  index do
    selectable_column
    id_column
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :confirmed_at
    actions
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs "Admin Details" do
      f.input :email
      f.input :name
      f.input :confirmed_at
      f.input :role, as: :radio
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

end
