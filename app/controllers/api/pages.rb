module API
  class Pages < Grape::API
    resource :pages do

      desc "Return a list of pages"
      get do
        Page.all
      end

      params do
        requires :token, type: String, desc: 'Page id or slug.'
      end
      route_param :token do
        desc 'Return a page using its slug or its id.'
        get do
          # Token isnt a positive integer
          if !/\A\d+\z/.match(params[:token])
            Page.find_by_slug(params[:token])
          else
            Page.find(params[:token].to_i)
          end
        end
      end
    end
  end
end
