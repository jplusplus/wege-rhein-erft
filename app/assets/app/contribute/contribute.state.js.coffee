angular.module 'wre'
  .config ($stateProvider) ->
    $stateProvider
      .state 'contribute',
        templateUrl: 'contribute/contribute.html'
        controller: 'ContributeCtrl'
        controllerAs: 'contribute'
        url: '/contribute'
        resolve:
          isAuthenticated: (Auth)->
            'ngInject'
            Auth.currentUser()
          inputs: (Restangular)->
            'ngInject'
            Restangular.all('inputs').withHttpConfig(cache: yes).getList()
