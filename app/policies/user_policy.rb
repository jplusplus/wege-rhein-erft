class UserPolicy  < AdminPolicy

  attr_reader :user, :model

  def initialize(user, model)
    @user = user
    @model = model
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      case @user.role
        when 'admin'
          scope.all
        else
          scope.where(role: 'contributor')
      end
    end
  end

  def destroy?
    @user and @user.role? :admin
  end

end
