module API
  class Base < Grape::API

    format :json
    rescue_from :all

    helpers Pundit
    helpers do
      def warden
        env['warden']
      end

      def current_user
        @current_user ||= warden.user
      end

      def authenticate!
        error!('401 Unauthorized', 401) unless current_user
      end
    end

    mount Places
    mount PlaceAccessibilities
    mount PlaceCategories
    mount DisplayRules
    mount Inputs
    mount Pages
    mount Surveys
  end
end
