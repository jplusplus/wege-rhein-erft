# See https://github.com/pitr/angular-rails-templates
Rails.application.config.angular_templates.module_name = 'wre.template'

Rails.application.config.ng_annotate.paths = [
  Rails.root.join('app/assets/app').to_s,
  Rails.root.join('app/assets/components').to_s
]

Rails.application.config.ng_annotate.ignore_paths = [
  Rails.root.join('vendor/').to_s
]
