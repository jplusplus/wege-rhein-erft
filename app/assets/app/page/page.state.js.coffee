angular.module 'wre'
  .config ($stateProvider) ->
    $stateProvider
      .state 'page',
        templateUrl: 'page/page.html'
        controller: 'PageCtrl'
        controllerAs: 'page'
        url: '/pages/:slug'
        resolve:
          page: ($stateParams, Restangular, filters)->
            'ngInject'
            Restangular.one('pages', $stateParams.slug).get()
