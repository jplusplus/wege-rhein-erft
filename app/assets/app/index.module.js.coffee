#= require_self
#= require_tree .

angular.module 'wre', [
  'ngAnimate',
  'ngCookies',
  'ngTouch',
  'ngSanitize',
  'ngAria',
  'restangular',
  'ui.router',
  'ui.bootstrap',
  'pascalprecht.translate',
  'wre.template',
  'angular-growl',
  'leaflet-directive',
  'wu.masonry',
  'angular-loading-bar',
  'duScroll',
  'Devise',
  '720kb.socialshare'
]
