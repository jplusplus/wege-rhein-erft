class Input < ActiveRecord::Base
  enum variable_type: { list: 'list', string: 'string', number: 'numer', boolean: 'boolean' }

  def to_s
    label
  end

  def group
    read_attribute(:group).titlecase
  end
end
