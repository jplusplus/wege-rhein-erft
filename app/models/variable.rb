class Variable
  include ActiveModel::Model

  attr_accessor :label, :name, :type, :slug
end
