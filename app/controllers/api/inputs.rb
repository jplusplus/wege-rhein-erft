module API
  class Inputs < Grape::API
    resource :inputs do

      desc "Return a list of inputs for contribute form"
      get do
        Input.all
      end

    end
  end
end
