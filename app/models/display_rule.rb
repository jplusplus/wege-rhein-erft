class DisplayRule
  include ActiveModel::Model

  attr_accessor :name, :type, :group, :rank, :label, :negative_label, :positive_label, :variable

  def label
    names.last
  end

  def names
    name.split('|').map(&:strip)
  end

  def rank
    @rank.to_i
  end

  def group
    names.first
  end
end
