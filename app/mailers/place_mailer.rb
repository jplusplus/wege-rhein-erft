class PlaceMailer < ApplicationMailer
  def pending_notification(place)
    @place = place
    User.where(:role => :admin).each do |user|
      mail(to: user.email, subject: "An approval is requested for the new place « #{@place.name} »")
    end
  end
end
