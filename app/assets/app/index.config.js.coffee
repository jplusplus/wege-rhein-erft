angular.module 'wre'
  .config ($logProvider, $httpProvider, RestangularProvider, cfpLoadingBarProvider)->
    # Enable log
    $logProvider.debugEnabled no
    # Set API root
    RestangularProvider.setBaseUrl '/api'
    # RestangularProvider.setDefaultHttpFields cache: yes
