class AdminPolicy  < ApplicationPolicy

  attr_reader :user, :record

  def initialize(user, record=nil)
    @user = user
    @record = record
  end

  def index?
    @user and @user.role? :admin, :editor
  end

  def show?
    index?
  end

  def create?
    index?
  end

  def import?
    create?
  end

  def new?
    create?
  end

  def update?
    create?
  end

  def destroy?
    create?
  end

  def destroy_all?
    destroy?
  end

end
