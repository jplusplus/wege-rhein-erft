module API
  class Surveys < Grape::API
    resource :surveys do

      desc "Return a list of ongoing surveys"
      get do
        Survey.ongoing
      end

    end
  end
end
