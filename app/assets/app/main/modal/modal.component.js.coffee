angular.module 'wre'
  .component 'mainModal',
    templateUrl: 'main/modal/modal.html'
    controllerAs: '$ctrl'
    bindings:
      resolve: '<'
      close: '&',
      dismiss: '&'
    controller: ($cookies)->
      @$onInit = -> angular.extend @, @resolve
      # Close and save the user request to show this modal again (or not)
      @ok = ->
        # User asked to hide this modal
        $cookies.put @cookie, 1 if @doNotShowAgain
        # Use the ui-bootstrap method
        do @close
      # Returns this
      @
