class AddKommentarToPlaces < ActiveRecord::Migration
  def change
    add_column :places, :kommentar, :string, default: ""
  end
end
