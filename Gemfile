source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.6'
gem 'rails-i18n', '~> 4.0.0'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# We use bootstrap as a gem to be able to customize it
gem 'bootstrap', '~> 4.0.0.alpha5'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby
gem 'sprockets-rails', '3.2.0'

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

gem "bower-rails", "~> 0.10.0"
gem 'puma'
gem 'devise', "~> 3.2"
gem 'devise-i18n'
gem "omniauth-google-oauth2"
gem 'omniauth-facebook'
gem 'pundit'
# Backend Interface
gem 'activeadmin', '~> 1.0.0.pre4'
gem 'active_admin_theme'
gem 'active_admin_import', '2.1.2'
gem 'enumerize'
gem 'geocoder'
gem "active_admin-sortable_tree"
# Rich text editor
gem 'rich', :git => 'https://github.com/kreativgebiet/rich.git'
gem 'paperclip'
# Better assets support
gem 'angular-rails-templates', '1.0.2'
gem 'ngannotate-rails'
gem 'sass-css-importer'
# API builder
gem 'grape'
# Email prerendering
gem 'premailer-rails'
gem 'nokogiri', '1.6.8'
# Page prerendering
gem 'prerender_rails'
gem 'activeadmin_addons'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  # Use sqlite3 as the database for Active Record
  gem 'sqlite3'
  # Better error display
  gem "better_errors"
  gem "binding_of_caller"
  # Assets livereload
  gem 'guard-livereload', '~> 2.5', require: false
  gem "rack-livereload"
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end

group :production do
  gem 'pg'
  gem 'rails_12factor'
end
