class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title, null: false, default: "", unique: true
      t.string :slug, null: false, default: "", unique: true
      t.text :body

      t.timestamps
    end
  end
end
