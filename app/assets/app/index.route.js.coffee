angular.module 'wre'
  .config ($stateProvider, $locationProvider, $urlRouterProvider, cfpLoadingBarProvider) ->
    'ngInject'
    $urlRouterProvider.otherwise '/'
    $locationProvider.hashPrefix '!'
    cfpLoadingBarProvider.includeSpinner = no
    cfpLoadingBarProvider.latencyThreshold = 0
  .run ($rootScope, $state, $location, $window)->
    'ngInject'
    # Stop progress indicator
    $rootScope.$on "$stateChangeSuccess", ->
      # Google Analytics  exists
      if $window.ga
        # Send 'pageview' to Google Analytics
        $window.ga 'send', 'pageview', page: $location.url()
    # Stop progress indicator
    $rootScope.$on "$stateChangeError", ->
      # By default, go to the 404 state
      $state.go "main"
