class CreateInputs < ActiveRecord::Migration
  def up
    create_table :inputs do |t|
      t.string :label
      t.string :help_text
      t.string :variable
      t.string :variable_type
      t.string :group
      t.string :values
      t.string :parent
      t.integer :rank
    end
  end

  def down
    drop_table :inputs
  end
end
