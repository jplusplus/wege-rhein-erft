# Wege Rehein-Erft

<img src="https://imgur.com/OWSGzwQ.png" width="100%" />

## Download and unpack this app

Download an archive from the project repository:

    mkdir wre
    curl https://gitlab.com/jplusplus/wege-rhein-erft/repository/archive.tar.gz | tar xz -C wre --strip-components=1

## Install in `development`

The following guide explain how to install this app for development purpose.

### 0. Install system dependencies

This may change according to your OS. On Ubuntu you must install those header packages to be able to compile ruby dependencies:

    sudo apt-get install ruby-dev libpq-dev libsqlite3-dev

### 1. Setup packages

Assuming you already installed [Ruby], [Bundler] and [Bower] on your computer.

    make install


### 2. Setup and populate database

    bin/rake db:setup

### 3. Run

The app autoreloads when changing ruby files, or assets.

    make run

## Install inside a Docker for `production`

### Build the container

To ease the deployment of this app, we provide a Dockerfile to build the app
and get ready for production with a single command line:

    docker build -t wre .

### Run the instance

Use environments variables within your Docker host to configure the app (see the list bellow).

Assuming you named your container `wre` as above, simply run:

    docker run -p 3333:3000 -it wre bin/web

Your app is now listening on [localhost:3333](http://localhost:3333)

## Environment variables in `production`

This app must be configured with environment variables:

Name | Description | Examples
--- | --- | ---
<var>DATABASE_URL</var> | The database URI to use. | `postgres://localhost:5432/wre`
<var>SENDGRID_USERNAME</var> | If present, uses SendGrid as mailer. |
<var>SENDGRID_PASSWORD</var> | SendGrid passowrd. |
<var>FACEBOOK_APP_ID</var> | A Facebook app ID to sign in/sign up with Facebook |
<var>FACEBOOK_APP_SECRET</var> | A Facebook app secret token  |
<var>GOOGLE_CLIENT_ID</var> | A Google client ID to sign in/sign up with Google |
<var>GOOGLE_CLIENT_SECRET</var> | A Google secret token  |


[Ruby]: https://www.ruby-lang.org/en/documentation/installation/
[Bower]: http://bower.io/#install-bower
[Bundler]: http://bundler.io
