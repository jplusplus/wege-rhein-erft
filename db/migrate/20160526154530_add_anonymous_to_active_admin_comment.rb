class AddAnonymousToActiveAdminComment < ActiveRecord::Migration
  def up
    add_column :active_admin_comments, :author_name, :string, default: "", null: true
    add_column :active_admin_comments, :author_ip_hash, :string, default: "", null: false
  end

  def down
    remove_column :active_admin_comments, :author_name
    remove_column :active_admin_comments, :author_ip_hash
  end
end
