angular.module 'wre'
  .controller 'MainCtrl', (Auth, $http, $scope, $state, $translate, $timeout, $cookies, $uibModal, growl, Geocoder, places, settings, leafletData, leafletMapEvents, filters, surveys, welcome)->
    new class MainCtrl
      constructor: ->
        @filters = filters
        # Do not show survey yet
        @showSurvey = no
        # Bind click on markers
        $scope.$on 'leafletDirectiveMarker.click', @onMarkerClick
        # Copy all map's settings to the scope
        angular.extend @, settings.map
        # Add custom controls
        @controls = custom: [@locationControl()]
        # All places
        @places = angular.copy places
        # Draw initial places
        @draw places
        # Set current user
        Auth.currentUser().then (currentUser)=>
          # User was logged in, or Devise returned
          # previously authenticated session.
          @currentUser = currentUser
        # Start survey in 10 secondes (if any)
        @prepareSurvey surveys[0] if surveys.length
        # after a login, a hard refresh, a new tab
        $scope.$on 'devise:login', (event, u)=> @currentUser = u
        # Every change success will refresh the cluster
        $scope.$on "$stateChangeSuccess", @refreshClusters
        # Add current position button
        leafletData.getMap().then @addLabelsLayer
        # Open welcome modal if a welcome value is given
        do @openWelcomeModal if welcome?
        # Defered loading of the border
        $http.get(window.paths.geojson.landkreise, {cache: yes, responseType: 'JSON'}).then (res)=>
          @geojson =
            data: res.data
            style:
              fillColor: "#2A803F"
              fillOpacity: 0.1
              weight: 2
              opacity: 1
              color: '#2A803F'
              dashArray: '3'
      geocode: =>
        # Geocode the location
        Geocoder.place(@location).then (res)=>
          # Reset state
          $state.go 'main'
          # Fit bound to location
          @setCenter res.lat, res.lon, 16
          # Add a position marker
          @draw places,
            id: 'here'
            icon:
              iconUrl: window.paths.images.here
              iconSize: [10*2, 28*2]
            lat: Number res.lat
            lng: Number res.lon
        # Error !
        , =>
          # Reset state
          $state.go 'main'
          # And send a notification
          growl.error $translate.instant('main.geocode.error')
      # Search method, receives the value to look for and return a identify function
      search: _.curry (value, item)->
        # Token must not be case sensitive
        value = value.toLowerCase()
        # Check every searchable properties
        _.some settings.search.properties, (prop)->
          # At least one property must match with the token
          String(item[prop]).toLowerCase().indexOf(value) > -1
      # Load places
      load: => filters.getPlaces().then @draw
      # Load places
      clear: => filters.clear().then @draw
      # Refresh the cluster to highlight active element
      refreshClusters: ->
        # Get the map
        leafletData.getMap().then (map)->
          # Iterate over every marker
          map.eachLayer (layer)->
            # Refresh cluster
            layer.refreshClusters? and do layer.refreshClusters
      locationControl: =>
        # Returns a new instance
        L.control.locate
          position: 'bottomleft'
          icon: 'fa fa-dot-circle-o'
          drawCircle: no
          drawMarker: yes
          follow: no
          onLocationOutsideMapBounds: _.flow $scope.$apply.bind($scope), @locationOutsideMapBounds
          locateOptions: { maxZoom: 16 }
          strings: { title: $translate.instant('main.geolocation.title') }
      locationOutsideMapBounds: (error)=>
        # And send a notification
        growl.error $translate.instant('main.geolocation.outside')
        # Stop positioning
        @controls.custom[0].stop()
      openWelcomeModal: ()=>
        $uibModal.open
          component: 'mainModal'
          resolve:
            title: -> welcome.title
            body: -> welcome.body
            cookie: -> 'hide_welcome'
      addLabelsLayer: (map)=>
        layer = Tangram.leafletLayer
          attribution: '<a href="https://mapzen.com/tangram" target="_blank">Tangram</a>, &copy; OSM contributors'
          scene: window.paths.tangram.scene
        # Add the layer to the map
        map.addLayer layer
      # Draw places
      draw: (places = places, here = false)=>
        # Create an empty set of marker OR a single marker on the given location
        @markers = if here then here: here else {}
        # Add a marker for each place on the places layers
        for place in places
          @markers[place.id] =
            id:     place.id
            lat:    place.latitude
            lng:    place.longitude
            layer: 'places'
        # Update map center according to bounds
        do @fitBounds unless here or _.isEmpty(@markers) or do @hasPlace
      # Go to a given state
      go: $state.go
      hasPlace: ->
        $state.is('main.place')
      isPanelOpen: =>
        $state.is('main.place', id: @panelOpenOn)
      togglePanel: (id)=>
        @panelOpenOn = if @panelOpenOn is id then null else id
      showPanel: (id)=>
        @panelOpenOn = id
      # Center map on a given points
      setCenter: (lat, lng, zoom)=>
        leafletData.getMap().then (map)->
          if zoom? and map.getZoom() isnt zoom
            map.setZoomAround L.latLng(lat, lng), zoom
          else
            map.panTo L.latLng(lat, lng)
      # Shortcut method
      resetBounds: => @fitBounds @markers, yes
      # Fit map bounds according to its markers
      fitBounds: (markers = @markers, reset = no)=>
        # Bounds need this format
        bounds = _.map markers, (m)-> [m.lat, m.lng]
        # Get the map...
        leafletData.getMap().then (map)=>
          # New bounds !
          map.fitBounds bounds
          # Delete here if exists
          delete @markers.here if reset
      # On marker click, go to the main.place state
      onMarkerClick: (ev, args)=>
        $state.go 'main.place', id: args.modelName
      closeSurvey: (survey)=>
        @showSurvey = no
        # Memorize this action with a cookie
        $cookies.put "survey_#{survey.id}", 1
      closeAndStartSurvey: (survey)=>
        # Close the survey
        @closeSurvey survey
        # Open the survey
        window.location = survey.redirect_url
      # Start a survey if there is any
      prepareSurvey: (survey, delay=10000)=>
        @survey = survey
        # Wait a moment
        $timeout (=> @showSurvey = yes) , delay
