module API
  class PlaceCategories < Grape::API
    resource :place_categories do

      desc "Return a list of categories"
      get do
        PlaceCategoryController.names.map do |name|
          { name: name }
        end
      end

    end
  end
end
