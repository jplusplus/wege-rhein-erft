class AddCategoryToPlace < ActiveRecord::Migration
  def up
    add_column :places, :category, :string
  end

  def down
    remove_column :places, :category
  end
end
