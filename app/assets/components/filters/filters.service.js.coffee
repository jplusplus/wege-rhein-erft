angular.module 'wre'
  .service 'filters', ($q, $timeout, $rootScope, Restangular)->
    new class Filters
      activeAccessibilities: {}
      activeCategories: {}
      filtered: false
      constructor: ->
        # Prefetch datasets and create a promise
        @p = $q.all({
          accessibilities: Restangular.all('place_accessibilities').getList()
          categories: Restangular.all('place_categories').getList()
        # Prepare the datasets
        }).then (data)=>
          # Extend the current instance with the datasets object
          angular.extend @, data
      ready: =>
        # Promise resolving all datasets used to build filters
        @p
      getParams: =>
        params = {}
        # At least one accessibility or all
        if @countActiveAccessibilities() and @countActiveAccessibilities() < @accessibilities.length
          # Accessibility filter
          params.accessibilities = _.keys @activeAccessibilities
        # At least one category or all
        if @countActiveCategories() and @countActiveCategories() < @categories.length
          # Categories filter
          params.categories = _.keys @activeCategories
        # Returns all params
        params
      clear: =>
        @activeAccessibilities = {}
        @activeCategories = {}
        # Refresh places
        do @getPlaces
      getPlaces: =>
        # Are the places filtered?
        @filtered = @countActiveAccessibilities() + @countActiveCategories() > 0;
        # Get the place using active filter and return a promise
        Restangular.all('places').getList @getParams()
      countActiveAccessibilities: =>
        _.compact(_.values(@activeAccessibilities)).length
      countActiveCategories: =>
        _.compact(_.values(@activeCategories)).length
      isActiveAccessibility: (accessibility)=>
        @activeAccessibilities[accessibility] or @countActiveAccessibilities() is 0
      toggleAccessibility: (accessibility)=>
        if @activeAccessibilities[accessibility]?
          delete @activeAccessibilities[accessibility]
        else
          @activeAccessibilities[accessibility] = yes
        # Number of active elements changes
        $rootScope.$broadcast 'filters:change'
      isActiveCategory: (category)=>
        @activeCategories[category] or @countActiveCategories() is 0
      toggleCategory: (category)=>
        if @activeCategories[category]?
          delete @activeCategories[category]
        else
          @activeCategories[category] = yes
        # Number of active elements changes
        $rootScope.$broadcast 'filters:change'
