angular.module 'wre'
  # We use factory to define this constant in order to inject $state
  .factory 'settings', ($stateParams)->
    'ngInject'
    geolocation: navigator?.geolocation?.getCurrentPosition?
    search:
      properties: ["name", "branche", "adresse_or_plz", "adresse_or_strasse_und_hausnummer", "kategorie"]
    map:
      defaults:
        attributionControl: no
        zoomControlPosition: 'bottomleft'
        scrollWheelZoom: no
        minZoom: 10
      maxbounds:
        southWest:
          lat: 50.6106
          lng: 5.9947
        northEast:
          lat: 51.145
          lng: 7.51328
      center:
        lat: 50.8911869
        lng: 6.4610718
        zoom: 10
      labels:
        url: 'http://tile.mapzen.com/mapzen/vector/v1/places/{z}/{x}/{y}.json?api_key=mapzen-5G5fc2i&locality=city'
        style:
          clickable: no
          color: "#00D"
          fillColor: "#00D"
          weight: 1.0
          opacity: 0.3
          fillOpacity: 0.2
      layers:
        baselayers:
          positron:
            name: 'CartoDB.Positron'
            type: 'xyz'
            url: "//cartodb-basemaps-{s}.global.ssl.fastly.net/light_nolabels/{z}/{x}/{y}.png",
            options:
              attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, &copy; <a href="https://cartodb.com/attributions">CartoDB</a>'
              subdomains: 'abcd'
              minZoom: 0
              maxZoom: 18
              continuousWorld: no
              noWrap: yes
        overlays:
          places:
            name: 'Places',
            type: 'markercluster',
            visible: yes
            layerOptions:
              singleMarkerMode: yes
              polygonOptions:
                weight: 2
                color: "#FEB24C"
              iconCreateFunction: (cluster)->
                c = ['marker-cluster', 'marker-cluster-small']
                if $stateParams.id?
                  for marker in cluster.getAllChildMarkers()
                    if marker.options.id is ($stateParams.id * 1)
                      c.push 'marker-cluster-active'
                childCount = do cluster.getChildCount
                new L.DivIcon
                  html: '<div><span>' + childCount + '</span></div>'
                  className: c.join(' ')
                  iconSize: new L.Point(40, 40)
              spiderLegPolylineOptions:
                weight: 1.5
                color: "#FEB24C"
