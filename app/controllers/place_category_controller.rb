class PlaceCategoryController < CollectionController
  @source = './lib/place_categories.csv'
  @model = PlaceCategory
end
