# Preview all emails at http://localhost:3000/rails/mailers/active_admin_comment_mailer
class ActiveAdminCommentMailerPreview < ActionMailer::Preview
  def pending_notification
    comment = ActiveAdminComment.where(:resource_type => Place).last
    ActiveAdminCommentMailer.pending_notification(comment)
  end
end
