class ApplicationMailer < ActionMailer::Base
  default from: "bonjour@jplusplus.org"
  layout 'mailer'
end
