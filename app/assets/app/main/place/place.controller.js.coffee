angular.module 'wre'
  .controller 'MainPlaceCtrl', ($state, $timeout, growl,  place, settings, menu, displayRules, leafletData, $scope)->
    new class MainPlaceCtrl
      constructor: ->
        angular.extend @, place
        # Only rules for strings
        displayRules = _.filter displayRules, type: 'string'
        # Only rules with values
        displayRules = _.filter displayRules, (r)-> place[r.variable]? and place[r.variable] isnt ''
        # Regroup rules
        @displayGroups =  _.groupBy displayRules, 'group'
        # Change map bound using a parent method
        $scope.$parent.main.setCenter @latitude, @longitude, 16
        $scope.$parent.main.location = ''
        # Delete here if exists
        delete $scope.$parent.main.markers.here
        # When destroying this state, restore the map zoom
        $scope.$on '$destroy', ->
          if $state.current.name is 'main'
            do $scope.$parent.main.fitBounds
      isSpecialRule: (rule)->
        ['webseite', 'email'].indexOf(rule.variable) > -1
      sendComment: ->
        # Post the comment
        place.post('comment', @comment).then (comment)=>
          # Add the comment to the list
          place.comments.push comment
          # After a small delay...
          $timeout(->
            # Target the last comment
            el = angular.element '.main__place__body__comments__list__item:last'
            # Scroll to the comment down the container
            angular.element('.main__panel').scrollTo el, {}, 300
          , 500)
        # Empty the user comment (but save author name)
        @comment = author_name: @comment.author_name
