ActiveAdmin.register ActiveAdminComment, :as => 'PlaceComment' do
  permit_params :status, :body
  config.comments = false # Don't allow comments on comments

  menu priority: 2
  filter :body, :as => :string
  filter :status, :as => :select, :collection => ActiveAdminComment.status.options

  scope :all
  scope :pending, ->(scope){ scope.pending }

  controller do
    def scoped_collection
      super.where :resource_type => 'Place'
    end
  end

  batch_action :approve do |ids, inputs|
    ActiveAdminComment.find(ids).each do |comment|
      comment.status = :approve
      comment.save
    end
    redirect_to collection_path, :flash => { :success =>  "#{ids.length} comment(s) approved!" }
  end

  batch_action :reject do |ids, inputs|
    ActiveAdminComment.find(ids).each do |comment|
      comment.status = :reject
      comment.save
    end
    redirect_to collection_path, :flash => { :success =>  "#{ids.length} comment(s) rejected!" }
  end



  index do
    selectable_column
    id_column
    column 'Author', :author_or_name
    column 'Place', :resource
    column :body
    tag_column :status
    column :created_at

    actions
  end

  form do |f|
    f.inputs "Admin Details" do
      f.input :body
      f.input :status, as: :radio
    end
    f.actions
  end
end
