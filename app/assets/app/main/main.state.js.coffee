angular.module 'wre'
  .config ($stateProvider) ->
    $stateProvider
      .state 'main',
        templateUrl: 'main/main.html'
        controller: 'MainCtrl'
        controllerAs: 'main'
        url: '/'
        resolve:
          cookies: ($cookies)->
            # Get all cookies
            $cookies.getAll()
          welcome: (Restangular, cookies)->
            'ngInject'
            # Do not get the page if we already read the welcome screen
            Restangular.one('pages', 'welcome').get() if not cookies["hide_welcome"]?
          surveys: (Restangular, cookies)->
            'ngInject'
            Restangular.all('surveys').withHttpConfig(cache: yes).getList().then (surveys)->
              # Filter survey to exclude the one alreay in the cookies
              _.filter surveys, (survey)-> not cookies["survey_#{survey.id}"]?
          displayRules: (Restangular)->
            'ngInject'
            Restangular.all('display_rules').withHttpConfig(cache: yes).getList()
          prefetch: (filters)->
            'ngInject'
            filters.ready()
          places: (filters)->
            'ngInject'
            filters.getPlaces()
