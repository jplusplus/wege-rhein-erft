module API
  class Places < Grape::API
    resource :places do

      desc "Return a ligthweight list of places"
      get do
        places = policy_scope(Place)
        # Filter places by accessibility
        if params[:accessibilities]
          places = places.select do |place|
            accessibilities = place.accessibility_list.select do |accessibility|
              params[:accessibilities].split(',').include? accessibility
            end
            not accessibilities.length.zero?
          end
        end
        # Filter places by category
        if params[:categories]
          places = places.select do |place|
            categories = place.kategorie_list.select do |kategorie|
              params[:categories].split(',').include? kategorie
            end
            not categories.length.zero?
          end
        end
        # Serialize places
        places.map do |place|
          {
            id: place.id,
            name: place.name,
            branche: place.branche,
            adresse_or_plz: place.adresse_or_plz,
            adresse_or_strasse_und_hausnummer: place.adresse_or_strasse_und_hausnummer,
            kategorie: place.kategorie,
            latitude: place.latitude,
            longitude: place.longitude,
            accessibility_list: place.accessibility_list
          }
        end
      end

      desc "Create a new place"
      post do
        place = Place.new
        # Create an object containg the place attributes
        params.each do | key, value |
          # The key must be an attribute of place
          if Place.column_names.include? key
            place[key] = Place.as_inline_list(value)
          end
        end
        place.status = :pending
        place.save!
        place
      end

      params do
        requires :id, type: Integer, desc: 'Place id.'
      end
      route_param :id do
        desc 'Return a place.'
        get do
          policy_scope(Place).find(params[:id]).as_json(
            :methods => [:accessibility_list],
            :include => {
              :comments => {
                :only => [
                  :id,
                  :author,
                  :body,
                  :created_at,
                  :author_name,
                  :is_anonymous,
                  :status
                ],
                :methods => [:is_anonymous, :author_avatar]
              }
            }
          )
        end

        desc 'Add a comment to a place.'
        params do
          requires :body, type: String, desc: 'Comment text'
          optional :author_name, type: String, desc: 'Author name'
        end
        post :comment do
          # "author_name" is required for non connected user
          if not params[:author_name] and not current_user
             error! 'Author name required for anonymous user', 400
          else
            policy_scope(Place).find(params[:id])
            comment = ActiveAdminComment.create(
              :resource_type => Place,
              :resource_id => params[:id],
              :body => params[:body],
              :namespace => 'api',
              :status => (current_user && current_user.role?(:admin) ? :approve : :pending),
              :author => current_user || nil,
              :author_name => params[:author_name],
              :author_ip_hash => request.env["HTTP_X_FORWARDED_FOR"] || request.env['REMOTE_ADDR']
            )
            # Serialise comment
            comment.as_json({
              :only => [
                :id,
                :author,
                :body,
                :created_at,
                :author_name,
                :is_anonymous,
                :status
              ],
              :methods => [:is_anonymous, :author_avatar]
            })
          end
        end
      end

    end
  end
end
