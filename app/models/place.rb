class Place < ActiveRecord::Base
  extend Enumerize
  has_many :active_admin_comments, as: :resource

  enumerize :status, :in => [:pending, :approve, :reject], :default => :approve
  validates :name, :presence => true
  validates :adresse_or_strasse_und_hausnummer, :presence => true
  validates :adresse_or_plz, :presence => true

  after_create :send_pending_notification
  after_validation :geocode
  geocoded_by :address, lookup: :nominatim

  scope :approved, ->{ where(:status => :approve) }
  scope :pending, ->{ where(:status => :pending) }

  attr_accessor :kategorie_raw

  def to_s
    name
  end

  def address
    [adresse_or_strasse_und_hausnummer, adresse_or_plz].compact * " "
  end

  def geocode
    # Geocode only if there is no latitude and longitude
    super unless latitude? and longitude?
  end

  def comments
    active_admin_comments.includes(:author).where(:status => 'approve').order(:created_at)
  end

  def accessibility
    if not read_attribute(:accessibility) or read_attribute(:accessibility) == ""
      find_accessibility
    else
      read_attribute(:accessibility)
    end
  end

  def accessibility_list
    if accessibility
      accessibility.split(",").map(&:strip)
    else
      []
    end
  end

  def kategorie_list
    if kategorie
      kategorie.split(",").map(&:strip)
    else
      []
    end
  end

  def self.as_inline_list(val)
    if val.respond_to? :join
      val.join(',')
    else
      val
    end
  end

  def webseite
    url = read_attribute(:webseite)
    if url.present?
      # The url is correct
      if url.start_with? 'http'
        url
      # Add an http suffix
      else
        "http://#{url}"
      end
    end
  end

  def email
    if read_attribute(:email).present?
      # Remove space between emails
      read_attribute(:email).split(',').map(&:strip).join(',')
    end
  end

  def find_accessibility
    # Get every uniq accessibility (some may have the same name)
    match = PlaceAccessibilityController::uniq.select do |accessibility|
      # Get every groups for this accessibility (according to its name)
      accessibility.groups.all? do |group|
        # Get every categories' conditions for this group AND this accessibility name
        conditions = PlaceAccessibilityController::all.select do |s|
          s.group == group and s.name == accessibility.name
        end
        # True if any condition is true for this group (within this accessibility)
        conditions.any? do |condition|
          # We test the value of each variable for this place
          condition.variables.any? { |variable| read_attribute(variable) }
        end
      end
    end
    match[0] ? match.map(&:name) * ',' : nil # returns nil if no categories match
  end

  def self.human_attribute_name(attribute_key_name, options={})
    if var = VariableController::find(attribute_key_name)
      var.label
    else
      super
    end
  end

  protected
    def send_pending_notification
      if status == :pending
        PlaceMailer.pending_notification(self).deliver_now
      end
    end
end
