class Page < ActiveRecord::Base
  def slug=(s)
    write_attribute :slug, s.parameterize
  end

  def excerpt
    ActionView::Base.full_sanitizer.sanitize(body).truncate(140)
  end
end
