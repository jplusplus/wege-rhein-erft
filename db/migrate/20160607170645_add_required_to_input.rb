class AddRequiredToInput < ActiveRecord::Migration
  def change
    add_column :inputs, :required, :boolean, default: false
  end
end
