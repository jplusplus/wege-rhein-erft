class AddAccessibilityToPlace < ActiveRecord::Migration
  def up
    add_column :places, :accessibility, :string
  end

  def down
    remove_column :places, :accessibility
  end
end
