ActiveAdmin.register Page do
  permit_params :title, :slug, :body

  menu priority: 4

  index do
    selectable_column
    id_column
    column :title
    column 'Body', :excerpt
    column :created_at

    actions
  end

  form do |f|
    f.inputs "Admin Details" do
      f.input :title
      f.input :slug
      f.input :body, :as => :rich, :config => { :width => '76%', :height => '400px' }
    end
    f.actions
  end
end
