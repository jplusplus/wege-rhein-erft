class ActiveAdminCommentMailer < ApplicationMailer
  def pending_notification(comment)
    @comment = comment
    @place = comment.resource
    User.where(:role => :admin).each do |user|
      mail(to: user.email, subject: "An approval is requested for a new comment on « #{@place.name} »")
    end
  end
end
