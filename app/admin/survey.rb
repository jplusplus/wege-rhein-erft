ActiveAdmin.register Survey do
  permit_params :title, :body, :redirect_url, :start_at, :end_at


  index do
    selectable_column
    id_column
    column :title
    column 'Body', :excerpt
    column :redirect_url
    column :ongoing? do |survey|
      survey.ongoing? ? status_tag( "yes", :ok) : status_tag("no")
    end

    actions
  end
end
