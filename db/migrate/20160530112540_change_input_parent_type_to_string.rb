class ChangeInputParentTypeToString < ActiveRecord::Migration
  def up
    change_column :inputs, :parent, :string
  end

  def down
    change_column :inputs, :parent, :integer
  end
end
