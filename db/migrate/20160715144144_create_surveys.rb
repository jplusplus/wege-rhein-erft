class CreateSurveys < ActiveRecord::Migration
  def change
    create_table :surveys do |t|
      t.string :title
      t.text :body
      t.string :redirect_url
      t.date :start_at
      t.date :end_at
      t.timestamps null: false
    end
  end
end
