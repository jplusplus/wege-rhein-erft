DOCKER_NAME := wre
APP := wre

run:
		rails server

install:
		bundle install
		npm install
		bower install

deploy: build-docker tag-docker push-docker

config-env:
		$(eval CONFIG := $(shell heroku config -s -a ${APP} | awk '{print "-e " $$0 }') )

migrate-with-docker: config-env
		docker run ${CONFIG} -it ${DOCKER_NAME} bin/init

tag-docker:
		docker tag $(DOCKER_NAME) registry.heroku.com/$(APP)/web

push-docker:
		docker push registry.heroku.com/$(APP)/web

build-docker:
		docker build -t $(DOCKER_NAME) .

save-docker: build-docker
		docker save $(DOCKER_NAME) > $(DOCKER_NAME).tar

bundle-docker: save-docker
		gzip $(DOCKER_NAME).tar

run-docker: config-env
		docker run ${CONFIG} --user=root --dns=8.8.8.8  -p 3003:3000 -it ${DOCKER_NAME} bash
