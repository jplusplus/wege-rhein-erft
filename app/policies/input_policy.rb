class InputPolicy  < AdminPolicy

  attr_reader :user, :model

  def initialize(user, model)
    @user = user
    @model = model
  end

  def permitted_attributes
    if @user and @user.role? :editor
      [:help_text, :rank]
    elsif @user and @user.role? :admin
      [:label, :help_text, :variable, :variable_type,
       :group, :values, :parent, :rank]
    else
      []
    end
  end

  def index?
    true
  end

  def show?
    true
  end

  def create?
    @user and @user.role? :admin
  end

  def destroy?
    create?
  end
end
